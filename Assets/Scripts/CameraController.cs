﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class CameraController : MonoBehaviour
{
	private static CameraController instance;

	public static CameraController Instance
	{
		get
		{
			if (instance == null)
			{
				instance = FindObjectOfType<CameraController>();
			}

			return instance;
		}
	}

	public Vector3 CurrentCameraRotation
	{
		get { return currentCameraRotation; }
	}

	public float RotationSmoothFactor;
	public float MouseLookSensitivity;

	public bool LockYaw;

	public Vector2 PitchClamp = new Vector2(-75, 75);

	Vector3 targetCameraRotation;
	Vector3 currentCameraRotation;
	float defaultY;
	bool shaking;
	private Camera cameraObj;

	bool rotationControl = true;

	void Start()
	{
		cameraObj = GetComponentInChildren<Camera>();

		defaultY = transform.localPosition.y;
		targetCameraRotation = new Vector3(transform.eulerAngles.y, transform.eulerAngles.x);
		currentCameraRotation = new Vector3(transform.eulerAngles.y, transform.eulerAngles.x);

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void OnPreRender()
	{
		var matrix = cameraObj.worldToCameraMatrix;
		Shader.SetGlobalMatrix("MATRIX_I_V", matrix);
	}

	public void ReParent(Transform newParent, Vector3 localPos)
	{
		transform.SetParent(newParent);
		transform.localPosition = localPos;
	}

	private Vector3 savedTargetRotation;
	private Vector3 savedCurrentRotation;
	private Vector2 savedPitchClamp;
	private float savedSmoothFactor;
	private float savedSensitivity;
	private float savedFoV;

	public void PushSettings()
	{
		savedTargetRotation = targetCameraRotation;
		savedCurrentRotation = currentCameraRotation;
		savedPitchClamp = PitchClamp;
		savedSmoothFactor = RotationSmoothFactor;
		savedSensitivity = MouseLookSensitivity;
		savedFoV = cameraObj.fieldOfView;
	}

	public void PopSettings()
	{
		targetCameraRotation = savedTargetRotation;
		currentCameraRotation = savedCurrentRotation;
		PitchClamp = savedPitchClamp;
		RotationSmoothFactor = savedSmoothFactor;
		MouseLookSensitivity = savedSensitivity;
		cameraObj.fieldOfView = savedFoV;
	}

	public void ApplyStarSphereSettings()
	{
		targetCameraRotation = Vector3.zero;
		currentCameraRotation = Vector3.zero;
		PitchClamp = new Vector2(-35, 35);
		RotationSmoothFactor = 10.0f;
		MouseLookSensitivity = 1.0f;
		cameraObj.fieldOfView = 45;
	}

	public void UpdateRotation()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			rotationControl = !rotationControl;

		if (rotationControl)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			
			
			// CROSS PLATFORM INPUT REFACTORING
			//
			// Vector3 deltaMouse = new Vector3(
			// 	CrossPlatformInputManager.GetAxis("Horizontal_Look"),
			// 	CrossPlatformInputManager.GetAxis("Vertical_Look"));
			

			Vector3 deltaMouse = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));


			targetCameraRotation += deltaMouse * MouseLookSensitivity;
			targetCameraRotation.y = Mathf.Clamp(targetCameraRotation.y, PitchClamp.x, PitchClamp.y);

			if (LockYaw)
				targetCameraRotation.x = Mathf.Clamp(targetCameraRotation.x, -40, 40);
		}
		else
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		currentCameraRotation = Vector3.Lerp(currentCameraRotation, targetCameraRotation, Time.deltaTime * RotationSmoothFactor);
		transform.rotation = Quaternion.AngleAxis(currentCameraRotation.x, Vector3.up) * Quaternion.AngleAxis(-currentCameraRotation.y, Vector3.right);

		if (shaking)
		{
			transform.localPosition = new Vector3(0, defaultY + Random.Range(-0.01f, 0.01f), 0);
		}
	}

	public static void Rotate(Vector3 deltaRotation)
	{
		Instance.targetCameraRotation += deltaRotation;
		Instance.currentCameraRotation += deltaRotation;
	}

	public static void StartShaking()
	{
		Instance.shaking = true;
	}

	public static void StopShaking()
	{
		Instance.shaking = false;
	}
}