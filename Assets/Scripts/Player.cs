﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
	public Vector3 Forward { get { return cameraTransform.forward; } }

	private CharacterController charController;
	[SerializeField]
	private Transform cameraTransform;
	[SerializeField]
	private CameraController cameraController;

	private float yForce = 0;
	private float walkSpeed = 8f;
	private float crouchSpeedFactor = 0.5f;
	private float jumpForce = 12.0f;
	private float stickToGroundForce = 2;
	private float gravity = 25;
	private bool crouched = false;
	private float cameraHeightNormal = 1.0f;
	private float cameraHeightCrouch = 0.5f;

	private void Awake() 
	{
		AbsoulutePosition.inOrigin = false;	
	}

	private void Start() 
	{
		charController = GetComponent<CharacterController>();
	}

	void Update()
	{
		Debug.Log("Origin :" + AbsoulutePosition.inOrigin);

		UpdateMovement();
		UpdateCrouch();
		cameraController.UpdateRotation();
	}

	void UpdateMovement()
	{
		var forward = Vector3.Cross(cameraTransform.right, Vector3.up).normalized;
		var right = Vector3.Cross(cameraTransform.forward, Vector3.up).normalized;
		var movement = Vector3.zero;

		// CROSS PLATFORM INPUT REFACTORING
		var VerticalInput = CrossPlatformInputManager.GetAxis("Vertical");
		var HorizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");
		VerticalInput = Mathf.Clamp(VerticalInput, -.1f, .1f);
		HorizontalInput = Mathf.Clamp(HorizontalInput, -.1f, .1f);

		bool forwardMove = VerticalInput >= 0.1f;
		bool backwardMove = VerticalInput <= -0.1f;
		bool rigthMove = HorizontalInput >= 0.1f;
		bool leftMove = HorizontalInput <= -0.1f;
		
		//MOBILE VERSION
		// movement += forwardMove || Input.GetKey(KeyCode.W) ? forward : Vector3.zero;
		// movement += backwardMove || Input.GetKey(KeyCode.S) ? -forward : Vector3.zero;

		// movement += rigthMove || Input.GetKey(KeyCode.D)? -right : Vector3.zero;
		// movement += leftMove || Input.GetKey(KeyCode.A) ? right : Vector3.zero;

		// PC VERSION
		movement += Input.GetKey(KeyCode.W) ? forward : Vector3.zero;
		movement += Input.GetKey(KeyCode.A) ? right : Vector3.zero;
		movement += Input.GetKey(KeyCode.S) ? -forward : Vector3.zero;
		movement += Input.GetKey(KeyCode.D) ? -right : Vector3.zero;
		movement = movement.normalized;
		movement *= walkSpeed * (crouched ? crouchSpeedFactor : 1.0f);

		

		if (charController.isGrounded)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				yForce = jumpForce;
            }
			else
			{
				yForce = -stickToGroundForce;
			}
		}
		else
		{
			yForce += -gravity * Time.deltaTime;
		}

		movement.y = yForce;

		charController.Move(movement * Time.deltaTime);
	}

	void UpdateCrouch()
	{
		crouched = Input.GetKey(KeyCode.LeftControl);
		var cameraPos = crouched ? new Vector3(0, cameraHeightCrouch) : new Vector3(0, cameraHeightNormal);
		cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, cameraPos, Time.deltaTime * 15);
	}

}