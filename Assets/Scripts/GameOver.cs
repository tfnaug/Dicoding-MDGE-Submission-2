﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour {

	private void Start () 
	{
		StartCoroutine(BackToMainMenu());
	}

	public IEnumerator BackToMainMenu()
	{
		yield return new WaitForSeconds(5);
		SceneManager.LoadScene("MainMenu");
	}

}
